package g3app

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import grails.testing.web.controllers.ControllerUnitTest
import groovy.util.logging.Slf4j
import spock.lang.Specification


@Mock([MyMember])
@TestFor(HomeController)
@Slf4j
class HomeControllerSpec extends Specification  {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
        given:'some transients'
        log.info "some log"
        log.info "transients; " + MyMember.transients
        //log.info "transients: " + MyMember.transients

        
        expect:"fix me"
        verify() == true

    }

    private boolean verify() {
        def f = (1..5).find {
            it > 5
        }
        f != null
    }
}
