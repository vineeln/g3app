quartz {
    autoStartup = true
    //jdbcStore = true

    //jobStore.isClustered = true

    plugin.triggerHistory.class='org.quartz.plugins.history.LoggingTriggerHistoryPlugin'
    plugin.triggerHistory.triggerFiredMessage='Trigger [{1}.{0}] fired job [{6}.{5}] scheduled at: {2, date, dd-MM-yyyy HH:mm:ss.SSS}, next scheduled at: {3, date, dd-MM-yyyy HH:mm:ss.SSS}'
    plugin.triggerHistory.triggerCompleteMessage='Trigger [{1}.{0}] completed firing job [{6}.{5}] with resulting trigger instruction code: {9}. Next scheduled at: {3, date, dd-MM-yyyy HH:mm:ss.SSS}'
    plugin.triggerHistory.triggerMisfiredMessage='Trigger [{1}.{0}] misfired job [{6}.{5}]. Should have fired at: {3, date, dd-MM-yyyy HH:mm:ss.SSS}'

    plugin.jobHistory.class='org.quartz.plugins.history.LoggingJobHistoryPlugin'
    plugin.jobHistory.jobToBeFiredMessage='Job [{1}.{0}] to be fired by trigger [{4}.{3}], re-fire: {7}'
    plugin.jobHistory.jobSuccessMessage='Job [{1}.{0}] execution complete and reports: {8}'
    plugin.jobHistory.jobFailedMessage='Job [{1}.{0}] execution failed with exception: {8}'
    plugin.jobHistory.jobWasVetoedMessage='Job [{1}.{0}] was vetoed. It was to be fired by trigger [{4}.{3}] at: {2, date, dd-MM-yyyy HH:mm:ss.SSS}'

}