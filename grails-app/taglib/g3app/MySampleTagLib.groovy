package g3app

import com.opensymphony.module.sitemesh.HTMLPage
import grails.artefact.TagLibrary

class MySampleTagLib implements TagLibrary {
    static namespace = "sample";
    static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    def allPageProperties = { attrs ->
        def renderTagLib = grailsApplication.mainContext.getBean('org.grails.plugins.web.taglib.RenderTagLib')
        HTMLPage page = getRequest().getAttribute("__sitemesh__page");
        log.info "page classs: ${page.getClass().getName()}"
        page.getPropertyKeys().each { key ->
            out << "<div>${key}</div>"
        }
    }
}