package g3app

import groovy.util.logging.Slf4j
import org.slf4j.Logger
import org.slf4j.LoggerFactory;

@Slf4j
class MyJob {

    final Logger jobLogger = LoggerFactory.getLogger(org.quartz.plugins.history.LoggingJobHistoryPlugin.getClass());

    static triggers = {
        simple repeatInterval: 100000
    }

    void execute() {

        log.info("is Info Enabled for: ${jobLogger.name}: ${jobLogger.isInfoEnabled()}")
        log.info("Executing Job !!!")


        //print "Job run!"
    }
}
