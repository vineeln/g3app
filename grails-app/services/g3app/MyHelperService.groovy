package g3app

import grails.gorm.transactions.Transactional
import groovy.util.logging.Slf4j

@Transactional
@Slf4j
class MyHelperService {

    def doMethod() {
        log.info("service: log class name: ${log.class.name}" )
        log.info("service log")
    }
}
