package g3app

import groovy.util.logging.Slf4j
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Slf4j
class HomeController {

    def myHelperService

    private final Logger myLog = LoggerFactory.getLogger(getClass());

    def index() {

        myHelperService.doMethod();

        log.info("controller: log name: ${log.name}")
        log.info("controller: myLog name: ${myLog.name}")

        Member m = new Member(name:'firstName')
        m.save(failOnError:true, flush:true)
        m.addToPhotos(new Photo(fileName: 'someName'))
        m.addToPhotos(new Photo(fileName: 'otherName'))
        m.save(failOnError:true, flush:true)


        def list = Member.findAll();
        

        render view: '/index'
    }
}
