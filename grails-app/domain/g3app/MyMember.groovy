package g3app

class MyMember {

    static constraints = {
    }
    static transients = [ 'fullName' ]

    String firstName
    String lastName
    String fullName
}
